//////////////////////////////////////////////////////////
var HEADER_STRING = "You May Like";
var MAX_TITLE_LENGTH_KR = 44;
var MAX_TITLE_LENGTH_JA = 43;
var MAX_TITLE_LENGTH_EN = 68;
var MAX_CATEGORY_LENGTH = 15;
var KOREAN = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/;
var JAPANESE = /[\u3000-\u303f]|[\u3040-\u309f]|[\u30a0-\u30ff]|[\uff00-\uff9f]|[\u4e00-\u9faf]|[\u3400-\u4dbf]/;
var API_IP_URL = "http://ip-api.com/json";
var API_TABOOLA_URL =
  "https://api.taboola.com/1.2/json/apitestaccount/recommendations.get?app.type=web&app.apikey=7be65fc78e52c11727793f68b06d782cff9ede3c&source.id=%2Fdigiday-publishing-summit%2F&source.url=https%3A%2F%2Fblog.taboola.com%2Fdigiday-publishing-summit%2F&source.type=text&amp;placement.organic-type=mix&amp;placement.visible=true&amp;placement.available=true&placement.rec-count=6&placement.name=Below%20Article%20Thumbnails&placement.thumbnail.width=640&placement.thumbnail.height=480&user.session=init";
//////////////////////////////////////////////////////////
var COUNTRY_LANGUAGE_CODE = {};
COUNTRY_LANGUAGE_CODE["KR"] = "ko";
COUNTRY_LANGUAGE_CODE["TH"] = "th";
COUNTRY_LANGUAGE_CODE["CN"] = "zh-Hans";
COUNTRY_LANGUAGE_CODE["IN"] = "hi";
COUNTRY_LANGUAGE_CODE["JP"] = "ja";
COUNTRY_LANGUAGE_CODE["US"] = "en";
COUNTRY_LANGUAGE_CODE["SG"] = "en";
COUNTRY_LANGUAGE_CODE["MY"] = "en";
COUNTRY_LANGUAGE_CODE["CA"] = "en";
COUNTRY_LANGUAGE_CODE["GB"] = "en";
//////////////////////////////////////////////////////////

window.onload = function () {
  findCountryWithIP();
  callTaboolaAPI();
};

function findCountryWithIP() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      var jsonResponse = JSON.parse(xhttp.responseText);
      country_code = jsonResponse.countryCode;
      //console.log(country_code);
      var language_code = COUNTRY_LANGUAGE_CODE[country_code];
      if (language_code != null) {
        translateHeaderString(HEADER_STRING, language_code);
      } else {
        renderHeader(HEADER_STRING);
      }
    }
  };
  xhttp.open("GET", API_IP_URL, true);
  xhttp.send();
}

function translateHeaderString(input_string, language_code) {
  var data = JSON.stringify([
    {
      Text: input_string,
    },
  ]);

  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === this.DONE) {
      var jsonResponse = JSON.parse(this.responseText);
      var translation = jsonResponse[0].translations;
      renderHeader(translation[0].text);
    }
  });

  xhr.open(
    "POST",
    "https://microsoft-translator-text.p.rapidapi.com/translate?to=" +
      language_code +
      "&api-version=3.0&profanityAction=NoAction&textType=plain"
  );
  xhr.setRequestHeader("content-type", "application/json");
  xhr.setRequestHeader(
    "x-rapidapi-key",
    "d9c554e7bcmshcf052cb55043a16p1cb03bjsn0fb6e0bf5e09"
  );
  xhr.setRequestHeader(
    "x-rapidapi-host",
    "microsoft-translator-text.p.rapidapi.com"
  );

  xhr.send(data);
}

function renderHeader(header_string) {
  var header_inline = document.getElementById("header_inline");
  header_inline.innerText = header_string;
}

function callTaboolaAPI() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      var jsonResponse = JSON.parse(xhttp.responseText);
      renderAll(jsonResponse.list);
      addEventHandler(jsonResponse.list);
    }
  };
  xhttp.open("GET", API_TABOOLA_URL, true);
  xhttp.send();
}

function shortenText(text, max_length) {
  var result = text;
  if (text.length > max_length) {
    result = text.substring(0, max_length - 3);
    result += "...";
  }
  return result;
}

function renderAll(list) {
  // title
  var titles = document.querySelectorAll(".item-container .item-title");
  Array.prototype.forEach.call(titles, function (title, index, array) {
    var original_title = list[index].name;
    var new_title = "";
    var MAX_TITLE_LENGTH = 0;
    if (KOREAN.test(original_title) == true) {
      MAX_TITLE_LENGTH = MAX_TITLE_LENGTH_KR;
    } else if (JAPANESE.test(original_title) == true) {
      title.style.fontSize = "14px";
      MAX_TITLE_LENGTH = MAX_TITLE_LENGTH_JA;
    } else {
      MAX_TITLE_LENGTH = MAX_TITLE_LENGTH_EN;
    }
    title.innerText = shortenText(original_title, MAX_TITLE_LENGTH);
  });

  // branding
  var brands = document.querySelectorAll(".item-container .item-branding");
  Array.prototype.forEach.call(brands, function (brand, index, array) {
    var brand_name = list[index].branding;
    brand.innerText = shortenText(brand_name, MAX_CATEGORY_LENGTH);
  });

  // category
  var categories = document.querySelectorAll(".item-container .item-category");
  Array.prototype.forEach.call(categories, function (category, index, array) {
    var category_name = list[index].categories;
    if (category_name != null) {
      category.innerText = "[" + category_name + "]";
    }
  });

  // thumbnails image
  var thumbnails = document.querySelectorAll(".item-container img");
  Array.prototype.forEach.call(thumbnails, function (thumbnail, index, array) {
    var img_url = list[index].thumbnail[0].url;
    thumbnail.src = img_url;
  });
}

function addEventHandler(list) {
  var items = document.querySelectorAll(".item-container .item");
  Array.prototype.forEach.call(items, function (item, index, array) {
    var page_url = list[index].url;
    item.onclick = function (e) {
      window.open(page_url);
    };
  });
}
